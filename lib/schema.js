const { GraphQLSchema } = require('graphql')

exports.schema = new GraphQLSchema({
  query: require('./query'),
})
