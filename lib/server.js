const { ApolloServer } = require('apollo-server')
const { schema } = require('./schema')

const server = exports.server = new ApolloServer({
  schema: schema,
  tracing: true,
})

void async function main(){
  const serverinfo = await server.listen(8090)
  console.log(serverinfo.url)
}()
