const graphql = require('graphql')
const { GraphQLString } = graphql

module.exports = /**@type {graphql.GraphQLFieldConfig<any,any>} */({
  type: GraphQLString,
  description: '测试服务是否正常',
  resolve: ()=>{
    return 'world'
  }
})
