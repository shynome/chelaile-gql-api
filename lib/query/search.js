const graphql = require('graphql')
const {
  GraphQLString,
  GraphQLNonNull,
} = graphql

const { Chelaile } = require('@/Chelaile')
const { GraphQLAny } = require('@/Scalar')
const {
  GraphQLSearchResult,
} = require('./Chelaile')

module.exports = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLSearchResult,
  description: '搜索',
  args: {
    'cityId': require('./cityId'),
    'keyword': {
      type: GraphQLNonNull(GraphQLString),
      description: '可搜索: 公交线路、车站、地点',
    }
  },
  /**@param {{cityId:string, keyword:string}} args */
  resolve: async (src,args)=>{
    const chelaile = new Chelaile(args.cityId)
    let res = await chelaile.search(args.keyword)
    return res
  }
})
