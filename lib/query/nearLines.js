const graphql = require('graphql')
const {
  GraphQLString,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLList,
} = graphql

const { Chelaile } = require('@/Chelaile')
const {
  GraphQLAny,
  GraphQLIntBoolean,
} = require('@/Scalar')
const {
  GraphQLInputPosition,
  GraphQLNearLine,
} = require('./Chelaile')

exports.LinesResponse = new GraphQLObjectType({
  name: 'LinesResponse',
  fields:{
    'toast': { type: GraphQLString },
    'ads': { type: GraphQLAny },
    'nearLines': { type: GraphQLList(GraphQLNonNull(GraphQLNearLine)) },
    'saveFav': {
      type: GraphQLIntBoolean,
      description: '是否已收藏',
    }
  }
})

exports.Lines = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLList(GraphQLNonNull(GraphQLNearLine)),
  description: '获取附近的公交线路',
  args: {
    'cityId': require('./cityId'),
    'position': { type: GraphQLNonNull(GraphQLInputPosition) },
  },
  /**@param {{cityId:string, position: ChelaileType.position }} args */
  resolve: async (src,args)=>{
    const chelaile = new Chelaile(args.cityId)
    let { nearLines:res } = await chelaile.getLines(args.position)
    return res
  }
})
