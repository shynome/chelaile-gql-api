const { GraphQLObjectType } = require('graphql')

module.exports = new GraphQLObjectType({
  name: 'query',
  fields:{
    'hello': require('./hello'),
    'cityList': require('./cityList').cityList,
    'line': require('./line').Line,
    'nearLines': require('./nearLines').Lines,
    'search': require('./search'),
  }
})
