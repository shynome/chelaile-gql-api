
const {
  GraphQLNonNull,
  GraphQLString,
} = require('graphql')

module.exports = {
  type: GraphQLNonNull(GraphQLString),
  description: '所在城市 id'
}