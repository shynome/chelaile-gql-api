const graphql = require('graphql')
const {
  GraphQLString,
  GraphQLList,
  GraphQLObjectType,
  GraphQLNonNull,
} = graphql

const { Chelaile } = require('@/Chelaile')
const { GraphQLIntBoolean } = require('@/Scalar')
const { GraphQLInputPosition } = require('./Chelaile')

exports.City = new GraphQLObjectType({
  name: 'City',
  fields: {
    'cityId': {
      type: GraphQLString
    },
    'cityName': {
      type: GraphQLString
    },
    'cityVersion': {
      type: GraphQLString,
      description: '奇怪的属性, 不知道车来了把它用到哪里了',
    },
    'isGpsCity': {
      type: GraphQLIntBoolean,
      description: 'GPS定位的城市'
    },
    'isHot': {
      type: GraphQLIntBoolean,
      description: '是否是热门城市'
    },
    'isSupport': {
      type: GraphQLIntBoolean,
      description: '车来了是否支持该城市 '
    },
    'pinyin': {
      type: GraphQLString,
      description: '城市名称的拼音. 车来了用来做拼音排序',
    },
    'supportSubway': {
      type: GraphQLIntBoolean,
      description: '该城市是否支持地铁',
    }
  }
})

exports.cityList = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLList(GraphQLNonNull(exports.City)),
  description: '获取城市列表',
  args: {
    'position': {
      type: GraphQLInputPosition,
    },
  },
  /**@param {{ position: {lat:string, lng: string} }} args */
  resolve: async (src,args)=>{
    let { data: { cityList } } = await Chelaile.getCityList(args.position)
    return cityList
  }
})
