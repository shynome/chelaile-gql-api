const {
  GraphQLString,
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLInt,
  GraphQLList,
} = require('graphql')
const { GraphQLAny } = require('../../Scalar/Any')

exports.GraphQLStation = new GraphQLObjectType({
  name: 'Station',
  description: '车站信息',
  fields:{
    'distanceToSp': {
      type: GraphQLInt,
      description: '不清楚用处'
    },
    'lat': { type: GraphQLFloat, },
    'lng': { type: GraphQLFloat, },
    'metros': {
      type: GraphQLList(GraphQLAny),
      description: '没找到对应数据'
    },
    'order': {
      type: GraphQLInt,
      description: '从开始数起的第几站'
    },
    'sId': {
      type: GraphQLString,
      description: 'StationId'
    },
    'sn': {
      type: GraphQLString,
      description: 'Station Name (车站名)',
    }
  }
})
