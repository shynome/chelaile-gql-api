
const {
  GraphQLInputObjectType,
  GraphQLFloat,
} = require('graphql')

exports.GraphQLInputPosition = new GraphQLInputObjectType({
  name: 'InputPosition',
  description: '经纬度, wsg 标准',
  fields: {
    'lat': { type: GraphQLFloat, description: '经度' },
    'lng': { type: GraphQLFloat, description: '纬度' },
  }
})
