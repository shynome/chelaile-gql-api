const {
  GraphQLString,
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
} = require('graphql')
const { GraphQLIntBoolean } = require('../../Scalar/IntBoolean')

exports.GraphQLBusTravel = new GraphQLObjectType({
  name: 'BusTravel',
  description: '车辆行驶信息',
  fields:{
    'arrivalTime': {
      type: GraphQLInt,
      description: '到达时间'
    },
    'debusCost': { type: GraphQLInt },
    'debusTime': { type: GraphQLInt },
    'order': {
      type: GraphQLInt,
      description: '站点排序',
    },
    'pRate': { type: GraphQLInt },
    'travelTime': {
      type: GraphQLInt,
      description: '不清楚干嘛的'
    }
  }
})

exports.GraphQLBus = new GraphQLObjectType({
  name: 'Bus',
  description: '车辆信息',
  fields:{
    'acBus': {
      type: GraphQLIntBoolean,
      description: '该车辆是否处于活动状态',
    },
    'beforeBaseIndex': {
      type: GraphQLInt,
      description: '上一次所在站点. -1 表示没有值',
    },
    'beforeLat': {
      type: GraphQLInt,
      description: '上一次定位. -1 表示没有值'
    },
    'beforeLng': {
      type: GraphQLInt,
      description: '上一次定位. -1 表示没有值'
    },
    'busBaseIndex': { type: GraphQLInt },
    'busDesc': {
      type: GraphQLString,
      description: '车辆描述'
    },
    'busDescList': {
      type: GraphQLList(GraphQLString),
    },
    'busId': {
      type: GraphQLString,
      description: '车牌号'
    },
    'delay': {
      type: GraphQLInt,
      description: '延时, 单位不清楚'
    },
    'distanceToSc': {
      type: GraphQLInt,
    },
    'lng': {
      type: GraphQLFloat,
      description: '车辆定位. -1 表示没有值'
    },
    'lat': {
      type: GraphQLFloat,
      description: '车辆定位. -1 表示没有值'
    },
    'licence': {
      type: GraphQLString,
      description: '车牌号'
    },
    'link': {
      type: GraphQLString,
    },
    'mTicket': {
      type: GraphQLInt
    },
    'order': {
      type: GraphQLInt,
      description: '该车辆所在站点位置',
    },
    'rType': { type: GraphQLInt },
    'shareId': { type: GraphQLString },
    'state': { type:GraphQLInt },
    'syncTime': {
      type: GraphQLInt,
    },
    'travels': {
      type: GraphQLList(GraphQLNonNull(exports.GraphQLBusTravel))
    },
    'userName': { type: GraphQLString },
    'userPhoto': { type: GraphQLString }
  }
})
