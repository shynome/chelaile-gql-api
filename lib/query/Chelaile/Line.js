const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
} = require('graphql')
const {
  GraphQLIntBoolean,
} = require('@/Scalar')

exports.GraphQLLine = new GraphQLObjectType({
  name: 'Line',
  description: '公交线路',
  fields:{
    'desc': {
      type: GraphQLString,
    },
    'shortDesc': {
      type: GraphQLString,
      description: '简短描述'
    },
    'direction': {
      type: GraphQLIntBoolean,
      description: '0 表示正方向, 1 表示逆方向'
    },
    'endSn': {
      type: GraphQLString,
      description: '终点站名字'
    },
    'startSn': {
      type: GraphQLString,
      description: '起点站名字'
    },
    'firstTime': {
      type: GraphQLString,
      description: '首班车发车时间',
    },
    'lastTime': {
      type: GraphQLString,
      description: '末班车发车时间'
    },
    'lineId': {
      type: GraphQLString,
      description: '线路ID',
    },
    'lineNo': {
      type:  GraphQLString,
      description: '看起来是线路内部编号',
    },
    'name': {
      type: GraphQLString,
      description: '线路名字, 第几路'
    },
    'price': {
      type: GraphQLString,
      description: '价格'
    },
    'sortPolicy': {
      type: GraphQLString,
      description: '排序规则. 值可能是: `flpolicy=0`'
    },
    'state': {
      type: GraphQLIntBoolean,
      description: '0 可用, 1 不可用',
    },
    'stationsNum': {
      type: GraphQLInt,
      description: '应该是车站编号. 只遇到了值: 21',
    },
    'type': {
      type: GraphQLInt,
      description: '未发现用处, 目前只遇到了 0 这一个值',
    },
    'version': {
      type: GraphQLString,
      description: '像是预留的版本号, 现在我遇到的都是空值',
    }
  }
})