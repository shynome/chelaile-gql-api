
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql')
const {
  GraphQLIntBoolean,
  GraphQLAny,
} = require('@/Scalar')
const {
  GraphQLLine,
} = require('./Line')
const { GraphQLStation } =  require('./Station')

exports.GraphQLWaitLine = new GraphQLObjectType({
  name: 'WaitLine',
  fields: {
    "fav": {
      type: GraphQLIntBoolean,
      description: '是否已收藏',
    },
    "favTagName": {
      type: GraphQLString,
    },
    "line": {
      type: GraphQLLine,
    },
    "nextStation": {
      type: GraphQLStation,
      description: '该公交的下一站'
    },
    "sortPolicy": {
      type: GraphQLString,
    },
    "stnStates": { type: GraphQLAny, },
    "targetStation": {
      type: GraphQLStation,
      description: '目标车站'
    }
  },
})

exports.GraphQLNearLine = new GraphQLObjectType({
  name: 'NearLine',
  fields:{
    'allLineSize': {
      type: GraphQLInt,
    },
    'canDel': {
      type: GraphQLIntBoolean,
    },
    'distance': {
      type: GraphQLInt,
      description: '与当前位置的距离'
    },
    'favsbLines': {
      type: GraphQLList(GraphQLAny),
    },
    'lines': {
      type: GraphQLList(GraphQLNonNull(exports.GraphQLWaitLine))
    },
    'sId': {
      type: GraphQLString,
      description: 'Station Id',
    },
    'sn': {
      type: GraphQLString,
      description: 'Station Name',
    },
    'sType': {
      type: GraphQLInt,
    },
    'sortPolicy': {
      type: GraphQLString,
      description: '不明所以得排序规则'
    },
    'subwayLines': {
      type: GraphQLAny,
    },
    'tag': {
      type: GraphQLString,
    }
  }
})
