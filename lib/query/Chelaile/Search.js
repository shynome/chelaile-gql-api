const {
  GraphQLString,
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
} = require('graphql')
const { GraphQLIntBoolean } = require('../../Scalar/IntBoolean')
const {
  GraphQLLine
} = require('./Line')
const {
  GraphQLStation
} = require('./Station')

exports.GraphQLPlace = new GraphQLObjectType({
  name: 'Place',
  description: '地点信息',
  fields:{
    'address': {
      type: GraphQLString,
      description: '地点名'
    },
    'sn':{
      type: GraphQLString,
      description: '站点名'
    },
    'lat': { type: GraphQLFloat }, 'lng': { type: GraphQLFloat },
  }
})

exports.GraphQLSearchResult = new GraphQLObjectType({
  name: 'SearchResult',
  description: '搜索结果. (注意: 搜索结果的只有一些关键的字段可用)',
  fields:{
    'lineCount':{
      type: GraphQLInt,
    },
    'lines': {
      type: GraphQLList(GraphQLNonNull(GraphQLLine)),
      description: '可用字段: endSn, lineId, lineNo, name'
    },
    'poiCount':{
      type: GraphQLInt,
    },
    'pois':{
      type: GraphQLList(GraphQLNonNull(exports.GraphQLPlace)),
      description: '地点列表.',
    },
    'stationCount':{
      type: GraphQLInt,
    },
    'stations':{
      type: GraphQLList(GraphQLNonNull(GraphQLStation)),
      description: '车站列表. 可用字段: sId, sn',
    },
    'ordertype':{
      type: GraphQLString,
      description: '数字合并的字符串',
    }
  }
})
