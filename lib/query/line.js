const graphql = require('graphql')
const {
  GraphQLString,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLInt,
  GraphQLList,
} = graphql

const { Chelaile } = require('@/Chelaile')
const {
  GraphQLIntBoolean,
} = require('@/Scalar')
const {
  GraphQLInputPosition,
  GraphQLStation,
  GraphQLBus,
  GraphQLLine,
} = require('./Chelaile')

exports.LineResponse = new GraphQLObjectType({
  name: 'LineResponse',
  description: '注意逆向班次的id不是同一个, 是 otherlines 里面的',
  fields:{
    'targetOrder': {
      type: GraphQLInt,
      description: '目标站点',
    },
    'line': { type: GraphQLLine },
    'otherlines': {
      type: GraphQLList(GraphQLNonNull(GraphQLLine)),
      description: '逆向班次的ID. 可用字段只有: lineId,startSn,endSn,firstTime,lastTime,price ',
      deprecationReason:''
    },
    'stations': { type: GraphQLList(GraphQLNonNull(GraphQLStation)) },
    'buses': { type: GraphQLList(GraphQLNonNull(GraphQLBus)) },
  }
})

exports.Line = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: exports.LineResponse,
  description: '获取某班车的具体信息. position 和 stationName 可以只传其中一个',
  args: {
    'cityId': require('./cityId'),
    'lineId': {
      type: GraphQLNonNull(GraphQLString),
    }, 
    'direction': {
      type: GraphQLIntBoolean,
      description: '这个字段是没有效果, 具体的方向是在 lineId 里就决定好了的',
      defaultValue: 0,
    },
    'position': {
      type: GraphQLInputPosition,
      description: '可以额外加上这个以便确定所在车站',
      defaultValue: {},
    },
    'stationName':{
      type: GraphQLString,
      description: '当前站点名字',
    },
  },
  /**@param {{cityId: string, lineId: string, position:ChelaileType.position, direction: 0|1}} args */
  resolve: async (src,args)=>{
    let chelaile = new Chelaile(args.cityId)
    let res = await chelaile.getLine(args)
    return res
  }
})
