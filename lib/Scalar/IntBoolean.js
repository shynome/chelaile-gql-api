
const {
  GraphQLScalarType,
} = require('graphql')

exports.GraphQLIntBoolean = new GraphQLScalarType({
  name: 'IntBoolean',
  description: '只有 0 和 1 这两个值',
  serialize: f=>f
})

