const {
  GraphQLScalarType,
} = require('graphql')

exports.GraphQLAny = new GraphQLScalarType({
  name: 'Any',
  description: '类型留到以后写',
  serialize: f=>f
})