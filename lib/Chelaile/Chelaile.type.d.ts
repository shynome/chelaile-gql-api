export as namespace ChelaileType

export type Response<T=any> = Promise<{
  data: T
  errmsg: string
  status: boolean
}>

export interface City {
  /**example: `'027'` */
  cityId: string
  /**example: `'北京'` */
  cityName: string
  /**奇怪的属性 */
  cityVersion: number,
  /**是否是GPS定位的城市 */
  isGpsCity: 1|0
  /**是否是热门城市 */
  isHot: 1|0
  /**车来了是否支持该城市 */
  isSupport: 1|0
  /**example: `''BeiJing` */
  pinyin: string
  /**是否支持地铁 */
  supportSubway: 1|0
}

export interface position {
  /**经度 */
  lat: number
  /**纬度 */
  lng: number
} 
