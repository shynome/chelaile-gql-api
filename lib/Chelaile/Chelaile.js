/// <reference path="./Chelaile.type.d.ts" />

const Req = require('./Req')
const axios = require('axios')

const jsonr = {
  start: '**YGKJ{"jsonr":'.length,
  end: -1*'}YGKJ##'.length,
}

class Chelaile {
  /**@param {string} cityId */
  constructor(cityId){
    this.params = {
      cityId: cityId,
    }
  }
  /**
   * @param {{lineId:string, direction?: 0|1, position?:ChelaileType.position, stationName?: string }} options 
   */
  async getLine( options ){
    let { lineId, position, direction=0, stationName } = options 
    let params = {
      ...this.params,
      lineId,
      direction,
      ...({stationName} || position || {}),
    }
    let data = await Chelaile.Req('api/bus/line!lineDetail.action',{ params })
    return data
  }
  /**
   * @param {ChelaileType.position} position
   */
  async getLines(position){
    let data = await Chelaile.Req('api/bus/stop!nearlines.action',{
      params: {
        ...this.params,
        ...position,
      }
    })
    return data
  }
  /**@param {string} keyword */
  async search(keyword){
    let data = await Chelaile.Req('api/basesearch/client/clientSearch.action',{
      params: {
        ...this.params,
        key: keyword,
      }
    })
    return data
  }
  /**
   * @template T
   * @param {string} url 
   * @param {axios.AxiosRequestConfig} options 
   * @returns {Promise<T>}
   */
  static async Req(url, options){
    let res = await Req(url, options)
    let data = /**@type {string} */(res.data)
    data = data.slice(jsonr.start,jsonr.end)
    let json = JSON.parse(data)
    return json.data
  }
  /**@param {{ lat:string, lng:string }} [position] */
  static async getCityList(position){
    let params = {}
    if(position && position.lat && position.lng){
      params = position
    }
    let res = await Req('wwd/ncitylist',{ params })
    /**@type {{status:'OK',data:{ cityList: ChelaileType.City[] }}} */
    let data = res.data
    return data
  } 
}

exports.Chelaile = Chelaile
