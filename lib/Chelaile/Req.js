

module.exports = require('axios').default.create({
  baseURL: 'https://web.chelaile.net.cn/',
  params: {
    s: 'h5',
    v: '3.3.19',
    src: 'webapp_weixin_mp',
    sign: 1,
    gpstype: 'wgs',
  },
  headers:{
    referer: 'https://web.chelaile.net.cn/ch5/index.html',
  },
})

